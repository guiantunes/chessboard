package chessboard;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.StringTokenizer;

public class Chessboard {
	
	private static final String BLACKS = "X";
	private static final String WHITES = "0";
	private static final String COL_SEPARATOR = " | ";
	private static final String PADDING = " ";

	public static void main(String[] args) throws IOException {
		
		// Reading the pattern
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Please enter the pattern: (CxL)");
		String s = br.readLine();
				
		StringTokenizer st = new StringTokenizer(s, "x");
		
		Integer patternCols = Integer.parseInt(st.nextToken());
		Integer patternLines = Integer.parseInt(st.nextToken());
		
		// Reading the board size
		System.out.println("Please enter the size of the board: ");
		s = br.readLine();
		
		Integer boardSize = Integer.parseInt(s);
		
		// Printing the board
		System.out.println("Printing board with size " + boardSize + " and a pattern " + patternCols + "x" + patternLines);
		System.out.println("");
		
		boolean blacks = true;
		boolean first = true;
		
		// Repeat for each board Line
		for (int i = 0; i < boardSize; i++) {
			// We will draw as many pattern lines depending on the size of the pattern lines
			for (int j = 0; j < patternLines; j++) {
				// if the line number is even, we start with blacks otherwise we start with whites
				if (i % 2 == 0) {
					blacks = true;
				} else {
					blacks = false;
				}
				// Mark first column
				first = true;
				// Drawing all the columns in this line
				for (int k = 0; k < boardSize; k++) {
					// Deciding if we are going to draw a black or a white column
					String patternToPrint = blacks ? BLACKS : WHITES;			
					// Draw the column with the correct color, size and delimiting the column where necessary
					printPattern (patternToPrint, patternCols, first);
					// Swapping color
					blacks = !blacks;
					// Marking not first column anymore
					if (first) {
						first = false;
					}
				}
				// Jumping to next line
				System.out.println("");
			}
			if (i != (boardSize - 1)){
				// print line delimiter
				for (int j = 0; j < ((patternCols * boardSize) + (COL_SEPARATOR.length() * (boardSize - 1)) + (PADDING.length() * 2)); j++) {
					System.out.print("-");								
				}
			}
			// Jumping to next line
			System.out.println("");
		}
	}

	// Convenient method to print the column
	private static void printPattern(String patternToPrint, Integer patternCols, boolean first) {

		// if not the first column in a line, draw a delimiter
		if(!first){
			System.out.print(COL_SEPARATOR);
		} else {
			System.out.print(PADDING);
		}
		// print the color pattern for as much as the pattern says
		for (int i = 0; i < patternCols; i++) {
			System.out.print(patternToPrint);
		}		
		
	}

}
